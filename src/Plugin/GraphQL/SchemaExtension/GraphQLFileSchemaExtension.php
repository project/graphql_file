<?php

namespace Drupal\graphql_file\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * A schema extension for File entities.
 *
 * @SchemaExtension(
 *   id = "graphql_file_extension",
 *   name = "GraphQL File",
 *   description = "Provides a File fields extension.",
 *   schema = "graphql_file"
 * )
 */
class GraphQLFileSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('File', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('File', 'title',
      $builder->fromPath('entity:file', 'filename.value')
    );

    $registry->addFieldResolver('File', 'url',
      $builder->compose(
        $builder->fromPath('entity:file', 'uri.value'),
        $builder->produce('file_url')
          ->map('uri', $builder->fromParent())
      )
    );
  }

}

# GraphQL File

**Project Status:** Unsupported

This is a small module designed to work with the Drupal File field type. It provides a GraphQL field type that can be used to return a File object.

## Obsolete Notice

**This module is now considered obsolete and is no longer maintained.**

We strongly recommend that users migrate to alternative solutions as this module will not receive any updates or support. Below are some recommended alternatives:

- [GraphQL Compose](https://www.drupal.org/project/graphql_compose)
- [GraphQL Core Schema](https://www.drupal.org/project/graphql_core_schema)

## Requirements

- [GraphQL](https://www.drupal.org/project/graphql) 4.x

## Usage

This module provides a GraphQL field type that can be used like this:

```gql
type MyType {
  documents: [File]
}
```

The field will return a File object with the following properties:

- id: The file entity id
- title: The file entity name
- url: The file entity url
